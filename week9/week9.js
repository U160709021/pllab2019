/*
 * This is a JavaScript Scratchpad.
 *
 * Enter some JavaScript, then Right Click or choose from the Execute Menu:
 * 1. Run to evaluate the selected text (Ctrl+R),
 * 2. Inspect to bring up an Object Inspector on the result (Ctrl+I), or,
 * 3. Display to insert the result in a comment after the selection. (Ctrl+L)
 */
a = parseInt(prompt("enter number"))
b = a + 2
console.log(b)
/*
b = "abc"
console.log(b)
*/

// a = 3
/*
Exception: TypeError: invalid assignment to const `a'
@Scratchpad/1:14:1
*/
if (a > 3){
  console.log("a greater than 3");
}
else if (a>7){
  console.log("a between 3 and 7")
}else{
  console.log("else")
}
