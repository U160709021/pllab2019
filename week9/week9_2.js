/*
 * This is a JavaScript Scratchpad.
 *
 * Enter some JavaScript, then Right Click or choose from the Execute Menu:
 * 1. Run to evaluate the selected text (Ctrl+R),
 * 2. Inspect to bring up an Object Inspector on the result (Ctrl+I), or,
 * 3. Display to insert the result in a comment after the selection. (Ctrl+L)
 */

for (a=0; a<10; a++){
  console.log(a)
}

a = [2,3,4,5,7,12]

for(let num of a){
  x = 1
  let y = 2
  console.log(num)
}
// console.log("last value", num)
/*
Exception: ReferenceError: num is not defined
@Scratchpad/2:19:1
*/
console.log(x)
// console.log(y)
/*
Exception: ReferenceError: y is not defined
@Scratchpad/2:27:1
*/