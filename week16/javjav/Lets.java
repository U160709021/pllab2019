import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Lets {

    public static void main(String args[]) {
        AVL<String> tree = new AVL<>();
/*
        tree.add(3);
        tree.add(4);

        System.out.println(tree.isEmpty());
        tree.remove(3);
        tree.remove(4);

        System.out.println(tree.isEmpty());

*/
/* heap insert logn
* */
        Scanner scanner = null;

        try {
            scanner = new Scanner(new File(args[0]));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("AVL TREE");

        long startTime2 = System.currentTimeMillis();
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            tree.add(line);
        }

        long endTime2 = System.currentTimeMillis();
        System.out.println("Loading took " + (endTime2 - startTime2) + " milliseconds");

        long startTime3 = System.currentTimeMillis();
        tree.remove("Mehmet");
        long endTime3 = System.currentTimeMillis();
        System.out.println("Removing took " + (endTime3 - startTime3) + " milliseconds");

        System.out.println("Binary Heap");

        BinaryHeap<String> pq = new BinaryHeap<>(true);

        try {
            scanner = new Scanner(new File(args[0]));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        long startTime = System.currentTimeMillis();

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            pq.add(line);
        }

        long endTime = System.currentTimeMillis();
        System.out.println("Loading took " + (endTime - startTime) + " milliseconds");

//        System.out.println("Length = " + pq.length());
////        System.out.println("Peek = " + pq.peek());
//        System.out.println(pq);


        long startTime1 = System.currentTimeMillis();
//        System.out.println("Remove:" + pq.remove("mehmet")); // buyuk harfleri once aliyor hepsini kucultup alabilirsin
//        System.out.println(pq);

        long endTime1 = System.currentTimeMillis();

        System.out.println("Removing took " + (endTime1 - startTime1) + " milliseconds");
    }
}
