"""Your program will accept an unsorted file of strings separated by EOL.
Load this list into a AVL data structure. Then load the same list into a Heap
data structure. Print how long each of insertions take. Then delete the
string "Mehmet" from each datastructure.  Print how long it takes to delete
from each data structure to compare which one is faster in terms of deletion.
"""

from avlTree import *
from heap import BinaryHeap
import timeit
import sys


with open(sys.argv[1]) as f:
    data = [line.rstrip('\n') for line in f]

tree = avltree()
bh = BinaryHeap()


def insertion(data):
    """ Insert data """
    for key in data:
        tree.insert(key)
#    print (tree.inorder_traverse())

print ('AVL load time: ', timeit.timeit("insertion(data)", globals=globals(), number=1))
print ('AVL data: ', tree.inorder_traverse())
print('Heap load time: ', timeit.timeit('bh.buildHeap(data)', globals=globals(), number=1))
print ('Heap data: ', bh)

print('AVL delete time: ', timeit.timeit('tree.delete("Mehmet")', globals=globals(), number=1))
print ('AVL deleted data: ', tree.inorder_traverse())
print('Heap delete time: ', timeit.timeit('bh.delete("Mehmet")', globals=globals(), number=1))
print ('Heap deleted data: ', bh)
